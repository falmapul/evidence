

Here's some more info about him:

If it’s the same guy (and if this is a legit info source):

https://voterrecords.com/voter/49988317/edgar-welch

Salisbury (NC) Post, 10/25/2016

Police ID driver of Harrison Road crash that seriously injured 13-year-old boy

http://www.salisburypost.com/2016/10/25/police-id-driver-harrison-road-crash-seriously-injured-13-year-old-boy/

FTA: The boy was taken to Wake Forest Baptist Medical Center where he remains in critical condition.

Edgar Welch, 28, of Salisbury, was traveling the posted 45 mph speed limit, officials said, but it’s still not immediately clear how the accident happened.

There’s an IMDB item for a guy by this name:

http://www.imdb.com/name/nm2625901/

